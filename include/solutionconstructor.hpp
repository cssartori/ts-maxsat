#ifndef __SOLUTION_CONSTRUCTOR_H
#define __SOLUTION_CONSTRUCTOR_H

#include "solution.hpp"
#include "datareader.hpp"

class SolutionConstructor{
private:
	static bool seeded; /*points whether a seed was already set or not*/
public:
	static Solution generateInitialSolutionFalse(DataReader data);
	static Solution generateInitialSolutionTrue(DataReader data);
	static Solution generateInitialSolutionRandom(DataReader data, int seed = 0, bool changeSeed = false);
	static void unseed();
};

#endif