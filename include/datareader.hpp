#define FILENAME_SZ 300

#ifndef __DATAREADER_H
#define __DATAREADER_H
#include "clause.hpp"


class DataReader {
private:
	std::vector< std::vector<int> > variablesClauseMap;
	std::vector<Clause> clauses;
	int numVariables;
	int numClauses;
	char filename[FILENAME_SZ];


public:
	DataReader();
	DataReader(const char* filename);
	bool read();
	std::vector<int> getVariableClauseMap(int id);
	Clause getClause(int index);
	int getNumVariables();
	int getNumClauses();

};

#endif