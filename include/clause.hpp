#ifndef __CLAUSE_H
#define __CLAUSE_H

#include <vector>

class Clause {
private:
	std::vector<int> variables_c;
	int size_c;

public:
	Clause(std::vector<int> variables_c);
	int size();
	std::vector<int> variables();
};

#endif