#ifndef __SOLUTION_H
#define __SOLUTION_H
#include <vector>

class Solution {
private:
	std::vector<bool> variables;
	std::vector<bool> clauseValues;
	int value_s;

public:
	Solution(int numVars, int numClauses);
	void updateValue(int addValue);
	int value();
	bool variable(int id);
	void setVariable(int id, bool value_v);
	bool clauseValue(int c);
	void setClauseValue(int c, bool value_c);

};

#endif