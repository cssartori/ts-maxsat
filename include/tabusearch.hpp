
#define MAX_ITERATIONS 100000
#define MAX_VARIABLES 3
#define ITERATIONS_LIST 15

#ifndef __TABU_SEARCH_H
#define __TABU_SEARCH_H
#include "solution.hpp"
#include "datareader.hpp"
#include <unordered_map>

class TabuSearch{
private:
	int seed;
	DataReader data;
	std::unordered_map <int, int> tabuList;

	Solution evaluateModifications(Solution s, int varId);
	bool isInTabuList(int varId, int iter);

public:
	TabuSearch(DataReader data, int seed);
	Solution search(Solution s0);

};

#endif