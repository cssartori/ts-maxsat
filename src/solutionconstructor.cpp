#include "../include/solutionconstructor.hpp"
#include <cstdlib>
#include <cstdio>

Solution evaluateClauses(Solution s, DataReader data);

bool SolutionConstructor::seeded = false;

Solution SolutionConstructor::generateInitialSolutionFalse(DataReader data){
	Solution s0(data.getNumVariables(), data.getNumClauses());

	/*Initiate variables values as 'false'*/
	for(int i=1;i<data.getNumVariables();i++){
		s0.setVariable(i, false);
	}

	s0 = evaluateClauses(s0, data);

	return s0;
}

Solution SolutionConstructor::generateInitialSolutionTrue(DataReader data){
	Solution s0(data.getNumVariables(), data.getNumClauses());

	/*Initiate variables values as 'false'*/
	for(int i=1;i<data.getNumVariables();i++){
		s0.setVariable(i, true);
	}

	s0 = evaluateClauses(s0, data);

	return s0;
}

Solution SolutionConstructor::generateInitialSolutionRandom(DataReader data, int seed, bool changeSeed){
	
	if(!seeded || changeSeed){
		srand(seed);
		seeded = true;
	}

	Solution s0(data.getNumVariables(), data.getNumClauses());

	/*Initiate variables values as 'false'*/
	for(int i=1;i<data.getNumVariables();i++){
		s0.setVariable(i, rand()%2);
	}

	s0 = evaluateClauses(s0, data);

	return s0;
}

void SolutionConstructor::unseed(){
	seeded = false;
}

Solution evaluateClauses(Solution s, DataReader data){

	for(int c=0;c<data.getNumClauses();c++){
		bool clauseValue = false;
		for(int i=0;i<data.getClause(c).size();i++){
			clauseValue = clauseValue || s.variable(data.getClause(c).variables()[i]);
		}

		s.setClauseValue(c, clauseValue);

		if(clauseValue)
			s.updateValue(1);
	}

	return s;
}