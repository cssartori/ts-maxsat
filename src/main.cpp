#include "../include/datareader.hpp"
#include "../include/solutionconstructor.hpp"
#include "../include/tabusearch.hpp"
#include <cstdio>

int main(){
	//DataReader data("./instances/max2sat/120v/s2v120c1200-1.cnf");
	
	DataReader data("./instances/max2sat/120v/s2v120c1600-1.cnf");
	//DataReader data("./instances/teste.cnf");
	data.read();

	int seed = 10009231;

	//Solution s0 = SolutionConstructor::generateInitialSolutionRandom(data, seed);
	Solution s0 = SolutionConstructor::generateInitialSolutionTrue(data);
	printf("Valor da solução inicial:  %i\n", s0.value());

	TabuSearch tb(data, seed);
	Solution sf = tb.search(s0);

	printf("Valor da solução final:  %i\n", sf.value());

	return 0;
}