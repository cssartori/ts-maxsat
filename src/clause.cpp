#include "clause.hpp"

Clause::Clause(std::vector<int> variables_c){
	this->variables_c = variables_c;
	this->size_c = variables_c.size();
}

int Clause::size(){
	return this->size_c;
}

std::vector<int> Clause::variables(){
	return this->variables_c;
}