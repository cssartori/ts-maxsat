#include "solution.hpp"


Solution::Solution(int numVars, int numClauses){
	this->variables.resize(numVars, false);
	this->clauseValues.resize(numClauses, false);
	this->value_s = 0;
}

void Solution::updateValue(int addValue){
	this->value_s+=addValue;
}

int Solution::value(){
	return this->value_s;
}

bool Solution::variable(int id){
	if(id < 0)
		return !this->variables[id*-1];
	else
		return this->variables[id];
}

bool Solution::clauseValue(int c){
	return this->clauseValues[c];
}

void Solution::setVariable(int id, bool value_v){
	if(id < 0){
		id = id*-1;
		value_v = !value_v;
	}
	this->variables[id] = value_v;
}

void Solution::setClauseValue(int c, bool value_c){
	this->clauseValues[c] = value_c;
}