#include "../include/tabusearch.hpp"
#include <iostream>
#include <utility>

TabuSearch::TabuSearch(DataReader data, int seed){
	this->data = data;
	this->seed = seed;
}

Solution TabuSearch::search(Solution s0){
	srand(this->seed);
	Solution sb = s0;
	int iter = 0;
	int numVarsChange = 1;

	while(iter < MAX_ITERATIONS){
		std::vector<int> varsChange;

		for(int i=0;i<numVarsChange;i++){
			int varId = rand()%this->data.getNumVariables();
			while(isInTabuList(varId, iter)){
				varId = rand()%this->data.getNumVariables();
			}
			varsChange.push_back(varId);
		}

		//printf("Changing var....%i\n", varId);
		for(auto& x : varsChange){
			tabuList.insert(std::make_pair(x,iter));

			s0.setVariable(x, !s0.variable(x));

			s0 = evaluateModifications(s0, x);
		}
		//printf("\tNew value:  %i\n", s0.value());
		if(s0.value() > sb.value())
			sb = s0;
		else{
			numVarsChange++;
			if(numVarsChange > MAX_VARIABLES)
				numVarsChange = 1;
		}


		iter++;
	}

	return sb;

}


bool TabuSearch::isInTabuList(int varId, int iter){
	if(this->tabuList.count(varId) != 0){
		if(iter - this->tabuList[varId] > ITERATIONS_LIST){
			this->tabuList.erase(varId);
			return false;
		}
		return true;
	}

	return false;
}


Solution TabuSearch::evaluateModifications(Solution s, int varId){

	std::vector<int> varMap = this->data.getVariableClauseMap(varId);

	for(int i=0;i<(int)varMap.size();i++){
		bool oldValue = s.clauseValue(varMap[i]);
		bool cValue = false;
		
		for(int j=0;j<this->data.getClause(varMap[i]).size();j++){
			cValue = cValue || s.variable(this->data.getClause(varMap[i]).variables()[j]);
		}

		if(oldValue != cValue && cValue == true){
			s.setClauseValue(varMap[i], cValue);
			s.updateValue(1);
		}
		else if(oldValue != cValue && cValue == false){
			s.setClauseValue(varMap[i], cValue);
			s.updateValue(-1);
		}

	}

	return s;

}