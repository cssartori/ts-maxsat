#include "datareader.hpp"
#include <string.h>
#include <stdio.h>
#include <cstdlib>

DataReader::DataReader(){
	
}

DataReader::DataReader(const char* filename){
	strncpy(this->filename, filename, FILENAME_SZ);
	this->filename[strlen(filename)+1] = '\0';
}

bool DataReader::read(){
	FILE *file = fopen(this->filename, "rt");
	
	if(!file) return false;

	const int lineSize = 100;
	char line[lineSize];
	
	int temp;

	/*Reads basic headers*/
	fgets(line, lineSize, file);
	fgets(line, lineSize, file);

	fscanf(file, "%s %s %i %i\n", line, line, &this->numVariables, &this->numClauses);
	this->numVariables+=1; /*0 is not a valid variable index*/
	this->variablesClauseMap.resize(this->numVariables);

	int n = 0;
	while(n < this->numClauses){
		std::vector<int> vars;
		//printf("%i: ", n);
		bool zeroFound = false;
		
		while(!zeroFound){
			fscanf(file, "%i ", &temp);
			
			if(temp == 0){
				zeroFound = true;
				continue;
			}else{
				vars.push_back(temp);
			}

			if(temp < 0) temp = temp*-1;
			this->variablesClauseMap[temp].push_back(n);
		}

		Clause c(vars);
		this->clauses.push_back(c);

		n++;
	}

	return true;
}

std::vector<int> DataReader::getVariableClauseMap(int id){
	if(id < 0) id = id*-1;
	return this->variablesClauseMap[id];
}

Clause DataReader::getClause(int index){
	return this->clauses[index];
}

int DataReader::getNumVariables(){
	return this->numVariables;
}

int DataReader::getNumClauses(){
	return this->numClauses;
}